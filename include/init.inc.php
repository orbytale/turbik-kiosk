<?
if (!defined('DOC_ROOT')) define('DOC_ROOT', get_docroot());

error_reporting(E_ALL);
require_once(DOC_ROOT . '/include/misc/db.inc.php');
require_once(DOC_ROOT . '/include/smarty/Smarty.class.php');
require_once(DOC_ROOT . '/include/misc/page.cls.php');
require_once(DOC_ROOT . '/include/misc/mycurl.cls.php');
require_once(DOC_ROOT . '/include/misc/turbik.cls.php');
require_once(DOC_ROOT . '/include/misc/functions.inc.php');


$tpl = new Page;
require_once(DOC_ROOT . '/include/init_page.inc.php');


function get_docroot()
{
	//get DOC_ROOT
	$droot = dirname(__FILE__);
	$droot = str_replace('\\', '/', $droot); //win-hack
	$droot = str_replace('/include', '', $droot);
	return $droot;
}

?>