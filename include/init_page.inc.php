<?
set_time_limit(0);
ini_set('max_execution_time', 0);

$config = db_getAll("SELECT * FROM config");
$_SERVER['conf'] = remap_array($config, 'key');
foreach ($_SERVER['conf'] as $k => $v) $_SERVER['conf'][$k] = $v['value'];

$last_series_check = get_last_check($_SERVER['conf']['last_series_checked']);
$tpl->assign('last_series_check', $last_series_check);


if (!empty($_SERVER['conf']['cookie'])) {
	$turbik = new turbik($_SERVER['conf']['cookie']);
	if (empty($turbik->auth)) $turbik = new turbik($_SERVER['conf']['turbik_login'], $_SERVER['conf']['turbik_pass']);
}
if (empty($turbik->auth)) redirect('/auth.php');


?>