<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty cat modifier plugin
 *
 * Type:     modifier<br>
 * Name:     cat<br>
 * Date:     Feb 24, 2003
 * Purpose:  catenate a value to a variable
 * Input:    string to catenate
 * Example:  {$var|cat:"foo"}
 * @link http://smarty.php.net/manual/en/language.modifier.cat.php cat
 *          (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @version 1.0
 * @param string
 * @param string
 * @return string
 */
function smarty_modifier_get_cat_name($id)
{
		$titles = array();
	    $cat = db_getRow("SELECT title, parent FROM categories WHERE id = $id LIMIT 1");
		$titles[] = $cat['title'];
		while ($cat['parent'] != '0' && $cat !== false) {
			if (empty($cat['parent'])) break;
			$cat = db_getRow("SELECT title, parent FROM categories WHERE id = ".$cat['parent']." LIMIT 1");
			$titles[] = $cat['title'];
		}
		$titles = array_reverse($titles);
		return implode(' > ', $titles);		
}

/* vim: set expandtab: */

?>
