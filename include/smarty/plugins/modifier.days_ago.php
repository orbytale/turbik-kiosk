<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

function smarty_modifier_days_ago($var)
{
		$my_date_lang = array(
			'ru' => array('today'=>'сегодня', 'yesterday'=>'вчера', 'monthes' => array(1=>'января',2=>'февраля',3=>'марта',4=>'апреля',5=>'мая',6=>'июня',7=>'июля',8=>'августа',9=>'сентября',10=>'октября',11=>'ноября',12=>'декабря')),
			'en'=> array('today'=>'today', 'yesterday'=>'yesterday', 'monthes' => array(1=>'januaru',2=>'february',3=>'march',4=>'april',5=>'may',6=>'june',7=>'july',8=>'august',9=>'september',10=>'october',11=>'november',12=>'december'))
		);
		if (strtolower(CUR_LANG) == 'ru') $my_lang = $my_date_lang['ru'];
		else $my_lang = $my_date_lang['en'];

		$today = time();
		$ts = intval($var);
		$days = floor( ($today - $ts) / 86400);
		if ($days == 0) return $my_lang['today'];
		if ($days == 1) return $my_lang['yesterday'];

		if (strtolower(CUR_LANG) == 'en') return $days.' days ago';
			else {
				$days = "$days";
				if (strlen($days) > 1) {
					$prelast = $days[ strlen($days) - 2];
					if ($prelast == '1') return $days.' дней назад';
				}
				$last = $days[ strlen($days) - 1];
				if ($last == '1') return $days.' день назад';

				$dney = array('2', '3', '4');
				if (in_array($last, $dney)) return $days.' дня назад';
					else return $days.' дней назад';


			}


}
/* vim: set expandtab: */
?>
