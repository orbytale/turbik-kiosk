<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

function smarty_modifier_get_link($string, $format = 'nohttp') {
	if ($format == 'nohttp') {
		$string = preg_replace('@^https?://@i', '', $string);
	} else {
		if ( !preg_match('@^https?://@i', $string) ) $string = 'http://'.$string;
	}
	return $string;
}
?>
