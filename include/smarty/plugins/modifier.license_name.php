<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

function smarty_modifier_license_name($var)
{
	if (!is_array($var)) {
		if ($var == '0') return '--';
		$var = intval($var);
		return db_getOne("SELECT CONCAT(type,'-',issuer,'-',id) FROM licenses WHERE id = '$var'", DB_USERS_NAME);
	} else return $var['type'].'-'.$var['owner'].'-'.$var['id'];
}
/* vim: set expandtab: */
?>
