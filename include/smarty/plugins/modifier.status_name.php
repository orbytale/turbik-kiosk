<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

function smarty_modifier_status_name($var)
{
	return (!empty($_SERVER['status_map'][$var])) ? $_SERVER['status_map'][$var]['name'] : '???';

/*	$var = strtolower($var);
		if ($var == 'inactive') return 'неактивен';
		elseif ($var == 'ba') return 'агент';
		elseif ($var == 'bp') return 'бизнес';*/
}
/* vim: set expandtab: */
?>
