<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

function smarty_modifier_my_date($string, $format = 'short', $default_date = '')
{
	$_SESSION['user']['timezone'] = -4;
		$my_date_lang = array(
			'ru' => array('today'=>'сегодня', 'yesterday'=>'вчера', 'monthes' => array(1=>'января',2=>'февраля',3=>'марта',4=>'апреля',5=>'мая',6=>'июня',7=>'июля',8=>'августа',9=>'сентября',10=>'октября',11=>'ноября',12=>'декабря')),
			'en'=> array('today'=>'today', 'yesterday'=>'yesterday', 'monthes' => array(1=>'januaru',2=>'february',3=>'march',4=>'april',5=>'may',6=>'june',7=>'july',8=>'august',9=>'september',10=>'october',11=>'november',12=>'december'))
		);
		if (strtolower(CUR_LANG) == 'ru') $my_lang = $my_date_lang['ru'];
		else $my_lang = $my_date_lang['en'];
		
		$timestamp = intval($string);
		if ($timestamp == 0) return '???';

		$now = utc_time();
		
		//apply user's timezone
    if (!empty($_SESSION['user']['timezone']) && $timestamp == intval($timestamp)) {
    	$timestamp += $_SESSION['user']['timezone']*3600;
			$now += $_SESSION['user']['timezone']*3600;
    }
		switch ($format) {
			case 'short':
				$today = date('d.m.Y', $now);
				$yesterday = strtotime("-1 day", $now);
				$yesterday = date('d.m.Y', $yesterday);				
				$day = date('d.m.Y', $timestamp);
				
				if ($day == $today) {
					$res = $my_lang['today'].', '.date('H:i', $timestamp);
				} elseif ($day == $yesterday) {
					$res = $my_lang['yesterday'].', '.date('H:i', $timestamp);
				} else {
 				  $res = date('d [] Y, H:i', $timestamp);
 				  $res = str_replace('[]', $my_lang['monthes'][date('n', $timestamp)], $res);
				}
			break;
			case 'date':
			  $res = date('d [] Y', $timestamp);
			  $res = str_replace('[]', $my_lang['monthes'][date('n', $timestamp)], $res);
			break;
			case 'day_time':
				$today = date('d.m.Y', $now);
				$yesterday = strtotime("-1 day", $now);
				$yesterday = date('d.m.Y', $yesterday);				
				$day = date('d.m.Y', $timestamp);
				
				if ($day == $today) {
					$res = $my_lang['today'].', '.date('H:i', $timestamp);
				} elseif ($day == $yesterday) {
					$res = $my_lang['yesterday'].', '.date('H:i', $timestamp);
				} else {
 				  $res = date('d [], H:i', $timestamp);
 				  $res = str_replace('[]', $my_lang['monthes'][date('n', $timestamp)], $res);
				}
			break;			
		}				
		return $res;
}
/* vim: set expandtab: */
?>
