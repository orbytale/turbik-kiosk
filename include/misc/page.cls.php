<?

class Page extends Smarty {
	var $page_title;
	var $smarty;
	var $tpl;
	var $error;
	var $description;
	var $kwds;
	var $crumbs;
	var $has_rss = false;
	
	function Page(){
		global $lang_domains;
		
		parent::Smarty();
		//Configuring Smarty
		$this->template_dir = DOC_ROOT.'/tpl/';
		$this->compile_dir =  DOC_ROOT.'/tpl/tpl_c/';
		$this->left_delimiter = '{';
		$this->right_delimiter = '}';
		//$this->config_dir = SMARTY_CONFIG_DIR;
		$this->caching = false;
		$this->force_compile = true;	
		//$this->debugging = true;
		$this->crumbs = array();
				
		//our variables
		$this->tpl = '';
		$this->title = '';
		$this->error = '';
		$this->description = '';
		$this->kwds = '';
	}
	function init() {
		//content template
		if ($this->tpl != '') $this->assign('tpl', $this->tpl.'.tpl');
		//page title
		$this->assign('PAGE_TITLE', $this->title);
		//page description
		if ($this->description != '') {
			$this->assign('PAGE_DESCRIPTION', $this->description);
		}	elseif (defined('SITE_DESCRIPTION')) $this->assign('PAGE_DESCRIPTION', SITE_DESCRIPTION);

		if (defined('CUR_LANG')) $this->assign('CUR_LANG', CUR_LANG);
		if (defined('TOP_DOMAIN')) $this->assign('TOP_DOMAIN', TOP_DOMAIN);
		if (defined('DEV_SERVER')) $this->assign('DEV_SERVER', DEV_SERVER);

		if (!empty($_SESSION['user'])) $this->assign('user', $_SESSION['user']);

		if ($this->has_rss) {
			$uri = $_SERVER['REQUEST_URI'];
			$uri = preg_replace('@/[^/]*\.[^/]*$@', '/', $uri);
			if (!preg_match('@/$@', $uri)) $uri .= '/';
			$rss_url = 'http://'.$_SERVER['SERVER_NAME'].'/rss'.$uri.'feed.xml';
			$this->assign('RSS_URL', $rss_url);
		}

		$this->assign('ld', '{');
		$this->assign('rd', '}');
		$this->assign('crumbs', $this->crumbs);
	}
	
	function render($file_name = 'main.tpl') {
		echo $this->fetch($file_name);
		$_SERVER['_db']->close();
		die;
	}
	
	
	function fetch($file_name = 'main.tpl', $cache_id = NULL, $compile_id = NULL, $display = false) {
		$this->init();

		return parent::fetch($file_name);
	}
}
?>
