<?

class turbik
{
	var $c;
	var $auth;
	var $ias_cookie;

	function turbik($login, $pass = '')
	{
		$this->c = new MyCurl(true);
		$auth = false;
		if (empty($pass)) $auth = $this->check_cookie_auth($login);
		else $auth = $this->check_login_auth($login, $pass);


		$this->auth = $auth;
	}

	function check_cookie_auth($cookie_val)
	{
		$this->ias_cookie = $cookie_val;
		curl_setopt($this->c->ch, CURLOPT_COOKIE, 'IAS_ID=' . $cookie_val);
		$res = $this->c->get('https://turbik.tv/My/Messages/New/turbofilm');
		if (preg_match('@location: /Signin@', $res)) return false;
		return true;
	}

	function  check_login_auth($login, $pass)
	{
		$data = array(
			'login' => $login,
			'passwd' => $pass
		);
		$url = 'https://turbik.tv/Signin';

		$res = $this->c->post($url, $data);
		if (preg_match('@IAS_ID=(\w+);@', $res, $m)) {
			update_config('turbik_login', $login);
			update_config('turbik_pass', $pass);
			update_config('cookie', $m[1]);
			$this->ias_cookie = $m[1];
			return true;
		} else {
			return false;
		}
	}

	function update_serials_list()
	{
		$s = $this->c->get('https://turbik.tv/Series');
		preg_match_all('@<a href="/Series/[^"]*?">\s+<span class="serieslistbox">.*?</a>@is', $s, $m);
		$data = $m[0];
		if (empty($data)) die('no data');
		$res = array();

		foreach ($data as $d) {
			$a = array();
			if (preg_match('@<span class="serieslistboxen">(.*?)</span>@', $d, $m)) $a['title_en'] = $m[1];
			if (preg_match('@<span class="serieslistboxru">(.*?)</span>@', $d, $m)) $a['title_ru'] = $m[1];
			if (preg_match('@<span class="serieslistboxperstext">Сезонов: (.*?)</span>@', $d, $m)) $a['seasons'] = $m[1];
			if (preg_match('@<span class="serieslistboxperstext">Эпизодов: (.*?)</span>@', $d, $m)) $a['episodes'] = $m[1];
			if (preg_match('@<span class="serieslistboxperstext">Продолжительность серии: (.*?)</span>@', $d, $m)) $a['episode_length'] = $m[1];
			if (preg_match('@<span class="serieslistboxperstext">Премьера сериала: (.*?)</span>@', $d, $m)) $a['premier'] = $m[1];
			if (preg_match('@<span class="serieslistboxperstext">Жанр: (.*?)</span>@', $d, $m)) $a['genre'] = $m[1];
			if (preg_match('@<span class="serieslistboxdesc">(.*?)</span>@', $d, $m)) $a['description'] = $m[1];
			if (preg_match('@<a href="/Series/([^"]*?)">@', $d, $m)) $a['uri'] = $m[1];
			if (preg_match('@<img src="(.*?)"@', $d, $m)) {
				$uri = $m[1];
				$fname = basename($uri);
				if (!file_exists(DOC_ROOT . '/images/' . $fname)) {
					$s = file_get_contents('http:' . $m[1]);
					file_put_contents(DOC_ROOT . '/images/' . $fname, $s);
				}
				$a['img'] = $fname;
			}
			$res[] = $a;
		}

		foreach ($res as $s) {
			foreach ($s as $k => $v) $s[$k] = str_replace("'", "''", $v);
			$has = db_getOne("SELECT id FROM serials WHERE uri = '$s[uri]' ");
			if (empty($has)) db_query("INSERT INTO serials (" . implode(',', array_keys($s)) . ") VALUES ('" . implode("','", array_values($s)) . "')");
			else {
				$fields = array();
				foreach ($s as $k => $v) $fields[] = " `$k` = '$v' ";
				$q = "UPDATE serials SET " . implode($fields, ' , ') . " WHERE id = " . $has;
				db_query($q);
			}
		}

		update_config('last_series_checked', time());
	}


	function update_serial($sid)
	{
		$serial = db_getRow("SELECT * FROM serials WHERE id = $sid");
		if (!empty($serial)) {
			$s = $this->c->get('https://turbik.tv/Series/' . $serial['uri'] . '/Season1');
			preg_match_all('@<a href="/Series/' . $serial['uri'] . '/Season(\d+)">@', $s, $m);
			$seasons = $m[1];
			foreach ($seasons as $season) {
				$data = ($season == 1) ? $s : $this->c->get('https://turbik.tv/Series/' . $serial['uri'] . '/Season' . $season);
				$this->update_season($serial, $season, $data);
			}
		}

		db_query("UPDATE serials SET last_checked = " . time() . " WHERE id = $sid");
	}

	function update_season($serial, $season, $data)
	{
		$episodes = array();
		preg_match_all('@<a href="/Watch/\w+/Season\d+/Episode\d+">(.*?)</a>@is', $data, $m);
		$data = $m[0];
		foreach ($data as $d) {
			$a['serial_id'] = $serial['id'];
			$a['season'] = $season;
			if (preg_match('@<a href="/Watch/\w+/Season\d+/Episode(\d+)">@', $d, $m)) $a['episode'] = $m[1];
			if (preg_match('@<span class="sserieslistonetxten">(.*?)</span>@', $d, $m)) $a['title_en'] = $m[1];
			if (preg_match('@<span class="sserieslistonetxtru">(.*?)</span>@', $d, $m)) $a['title_ru'] = $m[1];
			$a['hd'] = (preg_match('@<span class="sserieshq"></span>@', $d)) ? 1 : 0;
			$a['srt_rus'] = (preg_match('@<span class="sseriesrsub"></span>@', $d)) ? 1 : 0;
			$a['srt_en'] = (preg_match('@<span class="sseriesesub"></span>@', $d)) ? 1 : 0;
			$a['snd_rus'] = (preg_match('@<span class="sseriesrsound"></span>@', $d)) ? 1 : 0;
			$a['snd_en'] = (preg_match('@<span class="sseriesesound"></span>@', $d)) ? 1 : 0;

			if (preg_match('@<img src="(.*?)"@', $d, $m)) {
				$uri = $m[1];
				$fname = basename($uri);
				@mkdir(DOC_ROOT . '/images/' . $serial['uri']);
				if (!file_exists(DOC_ROOT . '/images/' . $serial['uri'] . '/' . $fname)) {
					$s = file_get_contents('http:' . $m[1]);
					file_put_contents(DOC_ROOT . '/images/' . $serial['uri'] . '/' . $fname, $s);
				}
				$a['img'] = $fname;
			}

			$episodes[] = $a;
		}

		foreach ($episodes as $s) {
			foreach ($s as $k => $v) $s[$k] = str_replace("'", "''", $v);
			$has = db_getOne("SELECT id FROM episodes WHERE serial_id = $serial[id] AND season='$s[season]' AND episode='$s[episode]' ");
			if (empty($has)) db_query("INSERT INTO episodes (" . implode(',', array_keys($s)) . ") VALUES ('" . implode("','", array_values($s)) . "')");
			else {
				$fields = array();
				foreach ($s as $k => $v) $fields[] = " `$k` = '$v' ";
				$q = "UPDATE episodes SET " . implode($fields, ' , ') . " WHERE id = " . $has;
				db_query($q);
			}
		}
	}

	function get_episode_metadata($episode)
	{
		$since_check = time() - $episode['last_checked'];

		//auto update once per day
		if (empty($episode['metadata']) || $since_check > 24 * 3600) return $this->fetch_episode_metadta($episode);
		else return $episode['metadata'];
	}

	function fetch_episode_metadta($episode)
	{
		$series = db_getRow("SELECT * FROM serials WHERE id = " . $episode['serial_id']);
		$url = 'https://turbik.tv/Watch/' . $series['uri'] . '/Season' . $episode['season'] . '/Episode' . $episode['episode'];

		$s = $this->c->get($url);
		if (preg_match('@id="metadata" value="([^"]*?)"@', $s, $m)) {
			$data = urldecode($m[1]);
			db_query("UPDATE episodes SET metadata='$data', last_checked='" . time() . "' WHERE id = " . $episode['id']);
			return $data;
		}
		return '';
	}

	function decode_meta($meta)
	{
		$s = enc_replace($meta, 'd');
		$xml = base64_decode($s);
		$xml = str_replace('utf-16', 'utf-8', $xml);
		$xml = simplexml_load_string($xml);
		$json = json_encode($xml);
		$meta = json_decode($json, TRUE);

		return $meta;
	}

	function get_all_links($data)
	{
		$res = array();
		foreach ($data['sources2'] as $vid => $src) {
			if ($vid == 'default') $vid = 'sd';
			$a = array('src' => $vid);
			foreach ($data['langs'] as $lang => $enabled) {
				if (!empty($enabled)) {
					$a['lang'] = $lang;
					$a['link'] = $this->get_link($src, $lang, $data);
					$res[] = $a;
				}
			}
		}
		return $res;
	}

	function get_link($src, $lang, $meta)
	{
		$start = 0; //timing
		$cookie = $this->ias_cookie;
		$rand = "0.7324287206865847"; // rand(1,100000)
		$b = sha1($cookie . $rand);
		$a = sha1($b . "" . $meta['eid'] . "A2DC51DE0F8BC1E9");
		$stream = "//cdn.turbik.tv/" . sha1($lang) . "/" . $meta['eid'] . "/" . $src . "/" . $start . "/" . $cookie . "/" . $b . "/" . $a;
		return $stream;
	}
}

function enc_replace($g, $f)
{
	$c = '';
	$b = '';
	$a = 0;
	$e = array("2", "I", "0", "=", "3", "Q", "8", "V", "7", "X", "G", "M", "R", "U", "H", "4", "1", "Z", "5", "D", "N", "6", "L", "9", "B", "W");
	$d = array("x", "u", "Y", "o", "k", "n", "g", "r", "m", "T", "w", "f", "d", "c", "e", "s", "i", "l", "y", "t", "p", "b", "z", "a", "J", "v");

	$c = $d;
	$b = $e;


	for ($i = 0; $i < count($c); ++$i) {
		$g = enc_replace_ab($c[$i], $b[$i], $g);
	}

	return $g;
}

function enc_replace_ab($e, $d, $c)
{
	$c = preg_replace('@' . preg_quote($e, '@') . '@', '___', $c);
	$c = preg_replace('@' . preg_quote($d, '@') . '@', $e, $c);
	$c = preg_replace('@___@', $d, $c);

	return $c;
}

?>