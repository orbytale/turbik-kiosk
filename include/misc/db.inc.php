<?
if ( !file_exists(DOC_ROOT . '/include/_db/turbik') ) copy(DOC_ROOT . '/include/_db/turbik_empty', DOC_ROOT . '/include/_db/turbik');
$_SERVER['_db'] = new SQLite3(DOC_ROOT . '/include/_db/turbik');


function db_getAll($q) {
	$res = array();
	$result = $_SERVER['_db']->query($q);
	while ($row = $result->fetchArray(SQLITE3_ASSOC)) $res[] = $row;

	return $res;
}

function db_getRow($q) {
	$res = array();
	$result = $_SERVER['_db']->query($q);
	if ($row = $result->fetchArray(SQLITE3_ASSOC)) $res = $row;
	return $res;
}

function db_getCol($q) {
	$res = array();
	$result = $_SERVER['_db']->query($q);
	while ($row = $result->fetchArray(SQLITE3_NUM)) $res[] = $row[0];
	return $res;
}

function db_getOne($q) {
	$res = false;
	$result = $_SERVER['_db']->query($q);
	if ($row = $result->fetchArray(SQLITE3_NUM)) $res = $row[0];
	return $res;
}

function db_query($q) {
	//echo $q;
	$_SERVER['_db']->query($q);
}
?>