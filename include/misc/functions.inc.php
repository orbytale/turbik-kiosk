<?
define('MYSQL_DATE', 'Y-m-d');

	if (!function_exists('utf8_to_ascii')) {
		function utf8_to_ascii($s) {
			return $s;
		}
	}
	//HTTP-302 redirect
	function redirect($url) {
		header('Location: '.$url);
		die;
	}

	//HTTP-404 Error
	function throw_404() {
		global $tpl;
		if (empty($tpl)) {
			$tpl = new Page;
		}
		header("HTTP/1.1 404 Not Found");
		header("Status: 404 Not Found");
		if (!empty($tpl)) {
			$tpl->tpl = '404';
			$tpl->render();
		}
		die();
	}

	//download file from URL
	function download_file($url) {
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		$s = curl_exec($ch);
		curl_close($ch);
		return $s;
  }

  function gen_url($s) {
  	$s = utf8_to_ascii($s);
    $s = utf8_strtolower($s);

    $s = preg_replace('/[^a-z0-9]/', ' ', $s);
    $s = preg_replace('/\s+/', ' ', $s);
    $s = utf8_trim($s);
    $s = str_replace(' ', '-', $s);
    return $s;
  }

	function validEmail($email) {
	   $isValid = true;
	   $atIndex = strrpos($email, "@");
	   if (is_bool($atIndex) && !$atIndex)	   {
	      $isValid = false;
	   } else	   {
	      $domain = substr($email, $atIndex+1);
	      $local = substr($email, 0, $atIndex);
	      $localLen = strlen($local);
	      $domainLen = strlen($domain);
	      if ($localLen < 1 || $localLen > 64)	      {
	         $isValid = false;
	      } else if ($domainLen < 1 || $domainLen > 255)	      {
	         $isValid = false;
	      } else if ($local[0] == '.' || $local[$localLen-1] == '.')	      {
	         $isValid = false;
	      } else if (preg_match('/\\.\\./', $local))	      {
	         $isValid = false;
	      } else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))	      {
	         $isValid = false;
	      } else if (preg_match('/\\.\\./', $domain))	      {
	         $isValid = false;
	      } else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local))) {
	         if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local))) {
	            $isValid = false;
	         }
	      }
	   }
	   return $isValid;
	}

	function kwds_to_array($s) {
		$s = explode(',', $s);
		foreach ($s as $k=>$v) {
			$v = utf8_trim(utf8_strtolower($v));
			if ($v == '') unset($s[$k]);
			else $s[$k] = $v;
		}
		$s = array_unique($s);
		return $s;
	}

	function get_lang($keyname, $lang = false) {
		$lang = ($lang === false) ? CUR_LANG : $lang;
		$res = (isset($_SERVER['my_lang'][$lang][$keyname])) ? $_SERVER['my_lang'][$lang][$keyname] : '';
		return $res;		
	}
	
	function get_meta($page) {
		$res = array('meta_description'=>'', 'meta_kwds'=>'');
		if (!empty($_SERVER['my_meta'][CUR_LANG][$page.'_desc'])) $res['meta_description'] = $_SERVER['my_meta'][CUR_LANG][$page.'_desc'];
		if (!empty($_SERVER['my_meta'][CUR_LANG][$page.'_kwds'])) $res['meta_kwds'] = $_SERVER['my_meta'][CUR_LANG][$page.'_kwds'];
		return $res;
	}
	
	function set_meta($page) {
		global $tpl;
		$meta = get_meta($page);
		if ($meta !== false) {
			if (!empty($meta['meta_description'])) $tpl->description = $meta['meta_description'];
			if (!empty($meta['meta_kwds'])) $tpl->kwds .= ','.$meta['meta_kwds'];
		}
	}
	
	function resize_im($dir, $fname)  {
		$res  = false;
		$max_size['w'] = 170;
		$max_size['h'] = 170;
		$im = imagecreatefromstring( file_get_contents(DOC_ROOT.$dir.'/'.$fname) );
		if (!$im) return false;
		
		$w = imagesx($im);
		$h = imagesy($im);
		$th_w = $w;
		$th_h = $h;
		$move_w = $move_h = 0;
		$w = $h = 0;					
		$img_maxsize = 170;
		if ($th_w >= $th_h) {					
			//// Landscape Picture ////
				$h = $img_maxsize;
				$w = (($th_w * $h) / $th_h);
				$move_w = (($th_w - $th_h) / 2);
				$w = $img_maxsize;
				$th_w = $th_h;
			
		} else {		
				$w = $img_maxsize;
				$h = (($th_h * $w) / $th_w);
				$move_h = (($th_h - $th_w) / 2);
				$h = $img_maxsize;
				$th_h = $th_w;
		}		
		$resized = imageCreatetruecolor($w, $h);
		$created = fastimagecopyresampled($resized, $im, 0, 0, $move_w, $move_h, $w, $h, $th_w, $th_h);
		

		//Grab new image
		$res = imagejpeg($resized, DOC_ROOT.$dir.'/th-'.$fname, 80);
		return $res;
	}

	//*** work with uploaded file
	function parse_uploaded_file($fname, $dir, $resize = false, $max_size = false, $new_name = false) {
		$file = $_FILES[$fname];
		$notice = '';

		if ($file['size'] == 0) {
			$notice = 'You didn\'t select file to upload or its size is 0 bytes.';
		} elseif (!is_integer(strpos($file['type'],'image'))) {
			$notice = 'You are trying to upload a non-image file.';
		} else {
			$name = strtolower(trim($file['name']));
			preg_match('/(.*?)\.(\w+)$/', $name, $m);
			if ((isset($m[2])) && (strlen($m[2]) > 0)) {
				$fname = $m[1];
				$ext = $m[2];
				if (($ext != 'jpg') && ($ext != 'jpeg') && ($ext != 'gif') && ($ext != 'png') ) $notice = 'You are trying to upload a non-image file.';
			} else $notice = 'You are trying to upload a non-image file.';
		}

		if ($notice == '') {
			//get new name
			$type = strtolower($file['type']);
			if ($new_name) { $url = $new_name.'.'.$ext; }
			else { $url = gen_url($fname).'.'.$ext; }
			$tmpname = $file['tmp_name'];
			if (file_exists(DOC_ROOT.$dir.$url)) {
				for ($i = 0; $i<9999; ++$i) {
					if (!file_exists(DOC_ROOT.$dir.$i.'_'.$url)) {
						$url = $i.'_'.$url;
						break;
					}
				}
			}
			//*** check max size
			if (is_array($max_size)) {
				$type = strtolower($file['type']);
				if (strpos($type,'jpeg') > 0) {
					$mtype = 'jpeg';
					$im = @imagecreatefromJPEG($tmpname);
				} elseif (strpos($type,'/gif') > 0) {
					$mtype = 'gif';
					$im = @imagecreatefromGIF($tmpname);
				} elseif (strpos($type,'/bmp') > 0) {
					$mtype = 'bmp';
					$im = @imagecreatefromWBMP($tmpname);
				} elseif (strpos($type,'/png') > 0) {
					$mtype = 'png';
					$im = @imagecreatefromPNG($tmpname);
				}
				if (!$im) $notice = 'An error occured while uploading image.';
				else {
					$w = imagesx($im);
					$h = imagesy($im);
					if ($w > $max_size['w']) $notice = 'Too big width.';
					if ($h > $max_size['h']) $notice = 'Too big heigth.';
					if (filesize($file['tmp_name']) > $max_size['file_size']*1024) $notice = 'File is too big.';
				}

			}
			//*** resize
			if (is_array($resize) && isset($resize['w']) && isset($resize['h'])) {
				$type = strtolower($file['type']);
				if (strpos($type,'jpeg') > 0) {
					$mtype = 'jpeg';
					$im = @imagecreatefromJPEG($tmpname);
				} elseif (strpos($type,'/gif') > 0) {
					$mtype = 'gif';
					$im = @imagecreatefromGIF($tmpname);
				} elseif (strpos($type,'/bmp') > 0) {
					$mtype = 'bmp';
					$im = @imagecreatefromWBMP($tmpname);
				} elseif (strpos($type,'/png') > 0) {
					$mtype = 'png';
					$im = @imagecreatefromPNG($tmpname);
				}
				if (!$im) $notice = 'An error occured while uploading image.';
				else {
					$w = imagesx($im);
					$h = imagesy($im);
					if ($h > $w) {
						if ($h > $resize['h']) $rate = $resize['h']/$h;
					} else {
						if ($w > $resize['w']) $rate = $resize['w']/$w;
					}
					if (isset($rate)) {
						$new_w = ceil($w*$rate);
						$new_h = ceil($h*$rate);
						$resized = imageCreatetruecolor($new_w, $new_h);
						imagecopyresized($resized,$im,0,0,0,0,$new_w,$new_h,$w,$h);
						$im = $resized; unset($resized);
					}
					//Grab new image
					ob_start();
					switch ($mtype) {
						case 'jpeg': ImageJPEG($im); break;
						case 'gif': ImageGIF($im); break;
						case 'bmp': ImageWBMP($im); break;
						case 'png': ImagePNG($im); break;
					}
					$image_buf = ob_get_contents();
					ob_end_clean();
					ImageDestroy($im);

					$fp = fopen(DOC_ROOT.$dir.$url, 'w');
					if ($fp === false) {
						$notice = "An error occured while copying image.";
					} else {
						fputs($fp, $image_buf);
						fclose($fp);
						$res['success'] = true;
						$res['url'] = $dir.$url;
						return $res;
					}
				}
			//*** just copy
			} elseif (empty($notice)) {
					if (!copy($file['tmp_name'], DOC_ROOT.$dir.$url)) {
						$notice = "An error occured while copying image.";
					} else {
						$res['success'] = true;
						$res['url'] = $dir.$url;
						return $res;
					}
			}

		}
		$res['success'] = false;
		$res['notice'] = $notice;
		return $res;
	}

	
	function microtime_float() {
   	list($usec, $sec) = explode(" ", microtime());
  	return ((float)$usec + (float)$sec);
	}

	function base64_urlencode($string) {
		$data = base64_encode($string);
		$data = str_replace(array('+','/','='),array('-','_','.'),$data);
		return $data;
	}
	function base64_urldecode($data) {
		$data = str_replace(array('-','_','.'),array('+','/','='),$data);
		$data = base64_decode($data);
		return $data;
	}

	function month_to_num($month) {
		$month = strtolower($month);
		$data = array (
		'january'=>1,
		'february'=>2,
		'march'=>3,
		'april'=>4,
		'may'=>5,
		'june'=>6,
		'july'=>7,
		'august'=>8,
		'september'=>9,
		'october'=>10,
		'november'=>11,
		'december'=>12,
		);
		return (isset($data[$month])) ? $data[$month] : 1;
	}

	function utc_time() {
		$now = time();
		$now -= $_SERVER['timezone']*3600;
		return $now;
	}

	function mask_room_login($str) {
		$n = round(strlen($str) / 4);
		$res = mask_string($str, $n, $n);
		return $res;
	}

	function mask_string($str, $first, $last) {
		$len = utf8_strlen($str);
		$res = $str;
		if ($len <= $first+$last) {
			$res = utf8_substr($str,0,1).str_repeat('*', $len-2).utf8_substr($str,$len-1,1);
		} else {
			$res = utf8_substr($str,0,$first).str_repeat('*', $len-($first+$last) ) .utf8_substr($str,$len-$last,$last);
		}
		return $res;
	}

	function craft_vcards($uid) {
		$user = db_getRow("SELECT * FROM users WHERE id=$uid", DB_USERS_NAME);
		if ($user === false) return;
		write_vcard('blog_author', $user);
		write_vcard('blog_comment', $user);
		write_vcard('forum_post', $user);
		write_vcard('username', $user);
		write_vcard('username_str', $user);
		write_vcard('forum_signature', $user);
		write_vcard('avatar', $user);
		
	}

	function write_vcard($type, $user) {
		global $tpl;
		$tpl->assign('vc_user', $user);
		$vcard = $tpl->fetch('vcards/'.$type.'.tpl');
		$vcard = str_replace('[#', '{#', $vcard);
		$vcard = str_replace('#]', '#}', $vcard);
		$fp = fopen(DOC_ROOT.'/vcards/'.$type.'/'.$user['id'].'.tpl', 'w');
		fputs($fp, $vcard);
		fclose($fp);
	}

	function my_sendmail($to, $subj, $text, $from='') {
		$mail = new htmlMimeMail();
		$mail->setSubject($subj);
/*		$text_plain = preg_replace('/<br>/i', "[sep]", $text);
		$text_plain = preg_replace('@<br/>@i', "[sep]", $text_plain);
		$text_plain = preg_replace('@</p>@i', "[sep]", $text_plain);		
		$text_plain = strip_tags($text_plain);
		$text_plain = str_replace('&nbsp;', ' ', $text_plain);
		$text_plain = str_replace('[sep]', "\r\n", $text_plain);*/
		$mail->setSMTPParams(SMTP_HOST, SMTP_PORT, '4ofaces', true, EMAIL_FROM, SMTP_PASS);
		$mail->setHTML($text, strip_tags($text));
		$mail->setHeadCharset('UTF-8');
		$mail->setHtmlCharset('UTF-8');
		$mail->setTextCharset('UTF-8');
		$mail->setFrom(empty($from)? 'admin@pokermob.ru':$from);
		$mail->send(array($to),'smtp');
	}

	function show_cards($text) {
		$text = str_replace(':diamond:', '<img src="http://4ofaces.com/images/d.png">', $text);
		$text = str_replace(':heart:', '<img src="http://4ofaces.com/images/h.png">', $text);
		$text = str_replace(':spade:', '<img src="http://4ofaces.com/images/s.png">', $text);
		$text = str_replace(':club:', '<img src="http://4ofaces.com/images/c.png">', $text);
		return $text;
	}

	
	function highlight_glossary_terms($text) {
		if (strpos(utf8_strtolower($text), '[term') === false) return $text;
		preg_match_all('@\[term=(.*?)\](.*?)\[/term\]@is', $text, $m);
		if (is_array($m[1])) {
			$kwds = array();
			foreach ($m[1] as $term) $kwds[] = "'".mysql_escape_string($term)."'";
			$kwds = implode(',', $kwds);
			$kwd_db = db_getAll("SELECT * FROM glossary WHERE title_kwd in ($kwds)");
			$kwd_data = array();
			if (is_array($kwd_db)) {
				foreach ($kwd_db as $k) $kwd_data[ utf8_strtolower($k['title_kwd']) ] = $k;
			}
			foreach ($m[0] as $k=>$v) {
				$str_to_replace = $m[0][$k];
				$kwd = utf8_strtolower($m[1][$k]);
				$term_str = $m[2][$k];
        if (isset($kwd_data[$kwd])) {
					 $data = preg_replace("/\r|\n/", ' ', $kwd_data[$kwd]['data']);
					 $data = str_replace("'", "&#39;", $data);
					 $data = str_replace('"', "&quot;", $data);
					 $replace_str = '<span class="a_glossary_term" onmouseover="Tip(\''.$data.'\', WIDTH, 250, DELAY, 50)">'.$term_str.'</span>';
        } else {
					$replace_str = $term_str;
				}
				$text = str_replace($str_to_replace, $replace_str, $text);
			}
		}
		return $text;
	}
	
	
	//*** work with uploaded file
	function parse_uploaded_photo($fname, $dir, $resize = false) {
		$file = $_FILES[$fname];
		$notice = '';

		if ($file['size'] == 0) {
			$notice = 'You didn\'t select file to upload or its size is 0 bytes.';
		} /*elseif (!is_integer(strpos($file['type'],'image'))) {
			$notice = 'You are trying to upload a non-image file.';
		} */else {
			$name = strtolower(trim($file['name']));
			preg_match('/(.*?)\.(\w+)$/', $name, $m);
			if ((isset($m[2])) && (strlen($m[2]) > 0)) {
				$fname = $m[1];
				$ext = $m[2];
				if (($ext != 'jpg') && ($ext != 'jpeg') && ($ext != 'gif') && ($ext != 'png') ) $notice = 'You are trying to upload a non-image file.';
			} else $notice = 'You are trying to upload a non-image file.';
		}

		if ($notice == '') {
			//get new name
			$type = strtolower($file['type']);
			$url = gen_url($fname).'.'.$ext; 
			$tmpname = $file['tmp_name'];
			if (file_exists(DOC_ROOT.$dir.$url)) {
				for ($i = 0; $i<9999; ++$i) {
					if (!file_exists(DOC_ROOT.$dir.$i.'_'.$url)) {
						$url = $i.'_'.$url;
						break;
					}
				}
			}
			$thumb_url = 'thumb_'.$url;
			$big_thumb_url = 'big_thumb_'.$url;

			//*** resize
			if (is_array($resize) && isset($resize['w']) && isset($resize['h'])) {
				if (isset($file['type']) && is_integer(strpos($file['type'],'image'))) {
					$type = strtolower($file['type']);
				} else {
					if ($ext == 'png') $type = 'image/png';
					elseif ($ext == 'gif') $type = 'image/gif';
					else $type = 'image/jpeg';
				}
				if (strpos($type,'jpeg') > 0) {
					$mtype = 'jpeg';
					$im = @imagecreatefromJPEG($tmpname);
				} elseif (strpos($type,'/gif') > 0) {
					$mtype = 'gif';
					$im = @imagecreatefromGIF($tmpname);
				} elseif (strpos($type,'/bmp') > 0) {
					$mtype = 'bmp';
					$im = @imagecreatefromWBMP($tmpname);
				} elseif (strpos($type,'/png') > 0) {
					$mtype = 'png';
					$im = @imagecreatefromPNG($tmpname);
				}
				if (!$im) $notice = 'An error occured while uploading image.';
				else {
					$w = imagesx($im);
					$h = imagesy($im);
					$th_w = $w;
					$th_h = $h;
					$move_w = $move_h = 0;
					$w = $h = 0;					
					$img_maxsize = 75;
					if ($th_w >= $th_h) {					
						//// Landscape Picture ////
							$h = $img_maxsize;
							$w = (($th_w * $h) / $th_h);
							$move_w = (($th_w - $th_h) / 2);
							$w = $img_maxsize;
							$th_w = $th_h;
						
					} else {		
							$w = $img_maxsize;
							$h = (($th_h * $w) / $th_w);
							$move_h = (($th_h - $th_w) / 2);
							$h = $img_maxsize;
							$th_h = $th_w;
					}		
					$resized = imageCreatetruecolor($w,$h);
					$created = fastimagecopyresampled($resized, $im, 0, 0, $move_w, $move_h, $w, $h, $th_w, $th_h);
					

					//Grab new image
					ob_start();
					switch ($mtype) {
						case 'jpeg': ImageJPEG($resized); break;
						case 'gif': ImageGIF($resized); break;
						case 'bmp': ImageWBMP($resized); break;
						case 'png': ImagePNG($resized); break;
					}
					$image_buf = ob_get_contents();
					ob_end_clean();
					ImageDestroy($resized);

					$fp = fopen(DOC_ROOT.$dir.$thumb_url, 'w');
					if ($fp === false) {
						$notice = "An error occured while copying image.";
					} else {
						fputs($fp, $image_buf);
						fclose($fp);
						$res['success'] = true;
						$res['thumb_url'] = $dir.$thumb_url;
					}
				}
			//*** prepare big thumb
			$w = imagesx($im);
			$h = imagesy($im);
			$img_maxsize_w = 620;
			$img_maxsize_h = 400;
			if ($w > $img_maxsize_w || $h > $img_maxsize_h) {
				if ($w >= $h) {					
						$th_w = $img_maxsize_w;
						$th_h = floor($h * ($img_maxsize_w/$w));
					
				} else {		
						$th_h = $img_maxsize_h;
						$th_w = floor($w * ($img_maxsize_h/$h));
				}		
				$resized = imageCreatetruecolor($th_w,$th_h);
				$created = fastimagecopyresampled($resized, $im, 0, 0, 0, 0, $th_w, $th_h, $w, $h);
	
				//Grab new image
				ob_start();
				switch ($mtype) {
					case 'jpeg': ImageJPEG($resized); break;
					case 'gif': ImageGIF($resized); break;
					case 'bmp': ImageWBMP($resized); break;
					case 'png': ImagePNG($resized); break;
				}
				$image_buf = ob_get_contents();
				ob_end_clean();
				ImageDestroy($resized);
	
				$fp = fopen(DOC_ROOT.$dir.$big_thumb_url, 'w');
				if ($fp === false) {
					$notice = "An error occured while copying image.";
				} else {
					fputs($fp, $image_buf);
					fclose($fp);
					$res['success'] = true;
					$res['big_thumb_url'] = $dir.$big_thumb_url;
				}
			} else $res['big_thumb_url'] = $dir.$url;
		}			
			//*** just copy			
			if (empty($notice)) {
					if (!copy($file['tmp_name'], DOC_ROOT.$dir.$url)) {
						$notice = "An error occured while copying image.";
					} else {
						$res['success'] = true;
						$res['url'] = $dir.$url;
						return $res;
					}
			}

		}
		$res['success'] = false;
		$res['notice'] = $notice;
		return $res;
	}	

	//*** work with uploaded file for forum/blog
	function parse_uploaded_file_img($fname, $dir, $resize = false) {
		$file = $_FILES[$fname];
		$notice = '';

		if ($file['size'] == 0) {
			$notice = 'You didn\'t select file to upload or its size is 0 bytes.';
		} else {
			$name = strtolower(trim($file['name']));
			preg_match('/(.*?)\.(\w+)$/', $name, $m);
			if ((isset($m[2])) && (strlen($m[2]) > 0)) {
				$fname = $m[1];
				$ext = $m[2];
				if (($ext != 'jpg') && ($ext != 'jpeg') && ($ext != 'gif') && ($ext != 'png') ) $notice = 'You are trying to upload a non-image file.';
			} else $notice = 'You are trying to upload a non-image file.';
		}

		if ($notice == '') {
			//get new name
			$url = gen_url($fname).'.'.$ext; 
			$tmpname = $file['tmp_name'];
			if (file_exists(DOC_ROOT.$dir.$url)) {
				for ($i = 0; $i<9999; ++$i) {
					if (!file_exists(DOC_ROOT.$dir.$i.'_'.$url)) {
						$url = $i.'_'.$url;
						break;
					}
				}
			}
			$big_thumb_url = 'thumb_'.$url;

			//*** resize
			if (is_array($resize) && isset($resize['w']) && isset($resize['h'])) {
				if (isset($file['type']) && is_integer(strpos($file['type'],'image'))) {
					$type = strtolower($file['type']);
				} else {
					if ($ext == 'png') $type = 'image/png';
					elseif ($ext == 'gif') $type = 'image/gif';
					else $type = 'image/jpeg';
				}
				if (strpos($type,'jpeg') > 0) {
					$mtype = 'jpeg';
					$im = @imagecreatefromJPEG($tmpname);
				} elseif (strpos($type,'gif') > 0) {
					$mtype = 'gif';
					$im = @imagecreatefromGIF($tmpname);
				} elseif (strpos($type,'bmp') > 0) {
					$mtype = 'bmp';
					$im = @imagecreatefromWBMP($tmpname);
				} elseif (strpos($type,'png') > 0) {
					$mtype = 'png';
					$im = @imagecreatefromPNG($tmpname);
				}
				if (!$im) $notice = 'An error occured while uploading image.';
				else {
					//*** prepare big thumb
					$w = imagesx($im);
					$h = imagesy($im);
					$img_maxsize_w = $resize['w'];
					$img_maxsize_h = 400;
					if ($w > $img_maxsize_w ) {
						$th_w = $img_maxsize_w;
						$th_h = floor($h * ($img_maxsize_w/$w));
						$resized = imageCreatetruecolor($th_w,$th_h);
						$created = fastimagecopyresampled($resized, $im, 0, 0, 0, 0, $th_w, $th_h, $w, $h);
			
						//Grab new image
						ob_start();
						switch ($mtype) {
							case 'jpeg': ImageJPEG($resized); break;
							case 'gif': ImageGIF($resized); break;
							case 'bmp': ImageWBMP($resized); break;
							case 'png': ImagePNG($resized); break;
						}
						$image_buf = ob_get_contents();
						ob_end_clean();
						ImageDestroy($resized);
			
						$fp = fopen(DOC_ROOT.$dir.$big_thumb_url, 'w');
						if ($fp === false) {
							$notice = "An error occured while copying image.";
						} else {
							fputs($fp, $image_buf);
							fclose($fp);
							$res['big_thumb_url'] = $dir.$big_thumb_url;
						}
					} 
				}
		}			
			//*** just copy			
			if (empty($notice)) {
					if (!copy($file['tmp_name'], DOC_ROOT.$dir.$url)) {
						$notice = "An error occured while copying image.";
					} else {
						$res['success'] = true;
						$res['url'] = $dir.$url;
						return $res;
					}
			}

		}
		$res['success'] = false;
		$res['notice'] = $notice;
		return $res;
	}		
	
	function fastimagecopyresampled (&$dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h, $quality = 3) {
 // Plug-and-Play fastimagecopyresampled function replaces much slower imagecopyresampled.
 // Just include this function and change all "imagecopyresampled" references to "fastimagecopyresampled".
 // Typically from 30 to 60 times faster when reducing high resolution images down to thumbnail size using the default quality setting.
 // Author: Tim Eckel - Date: 12/17/04 - Project: FreeRingers.net - Freely distributable.
 //
 // Optional "quality" parameter (defaults is 3).  Fractional values are allowed, for example 1.5.
 // 1 = Up to 600 times faster.  Poor results, just uses imagecopyresized but removes black edges.
 // 2 = Up to 95 times faster.  Images may appear too sharp, some people may prefer it.
 // 3 = Up to 60 times faster.  Will give high quality smooth results very close to imagecopyresampled.
 // 4 = Up to 25 times faster.  Almost identical to imagecopyresampled for most images.
 // 5 = No speedup.  Just uses imagecopyresampled, highest quality but no advantage over imagecopyresampled.
 
 if (empty($src_image) || empty($dst_image)) { return false; }
 if ($quality <= 1) {
   $temp = imagecreatetruecolor ($dst_w + 1, $dst_h + 1);
   imagecopyresized ($temp, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w + 1, $dst_h + 1, $src_w, $src_h);
   imagecopyresized ($dst_image, $temp, 0, 0, 0, 0, $dst_w, $dst_h, $dst_w, $dst_h);
   imagedestroy ($temp);
 } elseif ($quality < 5 && (($dst_w * $quality) < $src_w || ($dst_h * $quality) < $src_h)) {
   $tmp_w = $dst_w * $quality;
   $tmp_h = $dst_h * $quality;
   $temp = imagecreatetruecolor ($tmp_w + 1, $tmp_h + 1);
   imagecopyresized ($temp, $src_image, $dst_x * $quality, $dst_y * $quality, $src_x, $src_y, $tmp_w + 1, $tmp_h + 1, $src_w, $src_h);
   imagecopyresampled ($dst_image, $temp, 0, 0, 0, 0, $dst_w, $dst_h, $tmp_w, $tmp_h);
   imagedestroy ($temp);
 } else {
   imagecopyresampled ($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
 }
 return true;
}


function get_post_url($title = '') {
	if (empty($title)) $url = $orig = 'post-'.rand(1000,20000).'.html';
		else $url = $orig = gen_url($title).'.html';
	
	$n = 0;
	$test = db_getOne("SELECT id FROM blog_posts WHERE url='$url'", DB_USERS_NAME);
	while ($test !== false) {
		++$n;
		$url = str_replace('.html', '-'.$n.'.html', $orig);
		$test = db_getOne("SELECT id FROM blog_posts WHERE url='$url'", DB_USERS_NAME);
	}
	return  $url;	
}



	function registered_email($id) {
		global $tpl;
		
		$user = db_getRow("SELECT * FROM users WHERE id = $id", DB_USERS_NAME);
		$tpl->assign('em_user', $user);
		$text = $tpl->fetch('emails/registered.tpl');
		
		$mail = new htmlMimeMail();
		$mail->setSubject("Добро пожаловать");
		$mail->setHTML($text, strip_tags($text));
		$mail->setHeadCharset('UTF-8');
		$mail->setHtmlCharset('UTF-8');
		$mail->setTextCharset('UTF-8');
		$mail->send( array($user['email']) );
	}
	
	function getgval($var) {
		if (!empty($_GET[ $var ] )) return utf8_trim($_GET[ $var ]);
			else return '';
	}
	
	
	function  get_postget_var($key) {
		if (isset($_POST[$key])) return $_POST[$key];
		if (isset($_GET[$key])) return $_GET[$key];
		return '';
		
	}

	function get_survey_results($surveyid) {
        $sum = array();
        $a = db_getAll("SELECT answer_id, count(answer_id) as num FROM survey_votes WHERE survey_id= $surveyid GROUP BY answer_id");                
        $ttl = 0;
        if (is_array($a))  {
	        foreach ($a as $b) {
	            $ttl += $b['num'];
	            $sum[ $b['answer_id']  ] = $b['num'];
	        }
		}
        
        $answers = db_getAll("SELECT * FROM  survey_questions WHERE survey_id = $surveyid ORDER BY position ASC");
        foreach ($answers as $k=>$v) {
            $num = isset($sum[ $v['id'] ]) ? $sum[ $v['id'] ] : 0;
            $answers[$k]['votes'] = $num;
            $answers[$k]['percent'] = @round($num / $ttl * 100, 2);
            $answers[$k]['imgwidth'] = @round($answers[$k]['percent']);
        }
        return $answers;
    }
    
    function get_voted($surveyid) {
    	$voted = false;
    	if (!empty($_SESSION['user']['id'])) {
			$voted = db_getOne("SELECT answer_id FROM survey_votes WHERE survey_id = $surveyid AND user_id = '".$_SESSION['user']['id']."'");
			if ($voted !== false) return $voted;
    	}
    	if (!empty($_COOKIE['uid']))  $voted = db_getOne("SELECT answer_id FROM survey_votes WHERE survey_id = $surveyid AND cookie = '$_COOKIE[uid]'");
        if ($voted === false) {
        	$ip = !empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
			$ua = !empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
        	$voted = db_getOne("SELECT answer_id FROM survey_votes WHERE survey_id = $surveyid AND ip = '$ip' AND ua = '$ua'");        	
        }
        
        return $voted;		
    }

	function printr($a){
		print_r($a);
	}


	//!!! LOWER LVL INCLUDES HIGHER ONEs
	function get_net_stats($uid) {

		$net = get_user_childs( $uid );
		//	print_r($net);
		for ($i=0; $i<=10; ++$i) $net_stats[ $i ] = 0;

		foreach ($net as $u) {
			$net_stats[ $u['status'] ] = isset($net_stats['net'][ $u['status'] ]) ? ++$net_stats['net'][ $u['status'] ] : 1;
		}
		foreach ($net_stats as $k=>$v) {
			foreach ($net_stats as $kk=>$v) {
				if ($kk < $k) $net_stats[$kk] += $v;
			}
		}

		return $net_stats;
	}

	function get_user_childs($uid, $reg_type = '') {
		$users = db_getAll("SELECT * FROM users", DB_USERS_NAME);
		$tree = build_tree($users, 0);
		$flat = flatten_tree($tree);
		$childs = get_childs($flat, $uid, false);
		return $childs;
	}

	function get_childs($flat_tree, $id) {
		$parents = array($id);
		$res = array();

		if (!empty($flat_tree)) {
			foreach ($flat_tree as $f) {
				if (in_array($f['parent'], $parents)) {
					$res[] = $f;
					$parents[] = $f['id'];
				}
			}
		}

		return $res;
	}

	function get_child_cats($catid) {
		global $flat_tree;
		$res = array($catid);

		if (!empty($flat_tree)) {
			foreach ($flat_tree as $f) {
				if (in_array($f['parent'], $res)) {
					$res[] = $f['id'];
				}
 			}
		}

		return $res;
	}


function count_with_childs($a) {
	$res = count($a);
	foreach ($a as $b) {
		if (!empty($b['childs'])) $res += count_with_childs($b['childs']);
	}
	return $res;

}
function  build_tree($cats, $parent_id)
{
	$res = array();
	foreach ($cats as $cat) {
		if ($cat['parent'] == $parent_id) {
			$childs = build_tree($cats, $cat['id']);
			$cat['childs'] = $childs;
			$cat['n_personal_childs'] = count($childs);
			$cat['n_net_childs'] = count_with_childs($childs);
			$res[] = $cat;
		}
	}
	return $res;
}

function flatten_tree($cat_tree)
{
	$res = array();
	foreach ($cat_tree as $cat) {
		$c = $cat;
		unset($c['childs']);
		$res[] = $c;
		if (!empty($cat['childs'])) $res = array_merge($res, flatten_tree($cat['childs']));
	}
	return $res;
}

function only_authorized($requre_valid_license = false) {
	global $tpl;
	if (empty($_SESSION['user']['id'])) redirect('/');

	if ($requre_valid_license && !empty($_SESSION['user']['reg_type'])) redirect('/');

	if ($requre_valid_license && !is_valid_license( $_SESSION['user']['license_id'] ) ) {
		$tpl->tpl = 'require_license';
		$tpl->render();
		die;
	}
}

function only_authorized_tt($requre_valid_license = false) {
	global $tpl;
	if (empty($_SESSION['user']['id'])) redirect('/');

	$type = strtolower($_SESSION['user']['reg_type']);
	if ($type != 'tt') redirect('/');

	if ($requre_valid_license && !is_valid_license( $_SESSION['user']['license_id'] ) ) {
		$tpl->tpl = 'require_license_tt';
		$tpl->render();
		die;
	}
}

function only_authorized_bb($requre_valid_license = false) {
	global $tpl;
	if (empty($_SESSION['user']['id'])) redirect('/');

	$type = strtolower($_SESSION['user']['reg_type']);
	if ($type != 'bb') redirect('/');
}

function remap_array($a, $key) {
	$res = array();
	foreach ($a as $v) $res[ $v[$key] ] = $v;
	return $res;
}

function remap_arrays($a, $key) {
	$res = array();
	foreach ($a as $v) $res[ $v[$key] ][] = $v;
	return $res;
}


function myMail($to, $subject, $emailtpl, $vars = array() ) {
	global $tpl;

	foreach ($vars as $k=>$v) $tpl->assign($k, $v);
	$text = $tpl->fetch('emails/'.$emailtpl.'.tpl');

	$mail = new htmlMimeMail();
	$mail->setSubject( $subject );
	$mail->setHTML($text, strip_tags($text));
	$mail->setHeadCharset('UTF-8');
	$mail->setHtmlCharset('UTF-8');
	$mail->setTextCharset('UTF-8');
	$mail->send( array( $to ) );
}


function loadfiles($dir) {
	$res = array();
	$fs = scandir($dir);
	foreach ($fs as $f) {
		if ($f == '.' || $f == '..') continue;
		if ( is_file($dir.$f) ) $res[] = $f;
	}
	return $res;
}

function array_reset_keys($a) {
	$res = array();
	foreach ($a as $v) $res[] = $v;
	return $res;
}

function watermark_image($fn) {
	$wm = imagecreatefrompng(WATERMARK_IMG);
	$origi = imagecreatefromstring( file_get_contents($fn) );


	$ix = imagesx($origi);
	$iy = imagesy($origi);
	$wmx = imagesx($wm);
	$wmy = imagesy($wm);

	$i = imageCreatetruecolor($ix, $iy);
	$trans_colour = imagecolorallocate($i, 255, 255, 255);
	imagefill($i, 0, 0, $trans_colour);
	imagealphablending($i, true);
	imagecopy($i, $origi, 0, 0, 0, 0, $ix, $iy);

	$new_w = $ix;
	$new_h = round($wmy * ($new_w / $wmx));

	$resized = imageCreatetruecolor($new_w, $new_h);
	imagesavealpha($resized, true);
	$trans_colour = imagecolorallocatealpha($resized, 0, 0, 0, 127);
	imagefill($resized, 0, 0, $trans_colour);

	imagecopyresized($resized, $wm, 0, 0, 0, 0,$new_w, $new_h, $wmx, $wmy);


	$marge_right = 10;
	$marge_bottom = 10;
	$y = round( $iy / 2 - $new_h / 2);
	imagecopy($i, $resized, 0, $y, 0, 0, imagesx($resized), imagesy($resized));

	$new = preg_replace('@\.\w+$@', '_wmed.jpeg', $fn);
	imagejpeg($i, $new, 85);

	rename($fn, $fn.'.bak');
	rename($new, $fn);

}

function update_config($key, $value) {
	$value = escape_string($value);
	db_query("DELETE FROM config WHERE `key`='$key'");
	db_query("INSERT INTO config (`key`, `value`) VALUES ('$key', '$value')");
}

function escape_string($s) {
	return str_replace("'", "''", $s);
}

function get_last_check($last_check)
{
	if (empty($last_check)) return 'Никогда';
	else return diff_time($last_check);
}

function diff_time($time)
{
	$month_name =
		array(1 => 'января',
			2 => 'февраля',
			3 => 'марта',
			4 => 'апреля',
			5 => 'мая',
			6 => 'июня',
			7 => 'июля',
			8 => 'августа',
			9 => 'сентября',
			10 => 'октября',
			11 => 'ноября',
			12 => 'декабря'
		);

	$month = $month_name[date('n', $time)];

	$day = date('j', $time);
	$year = date('Y', $time);
	$hour = date('G', $time);
	$min = date('i', $time);

	$date = $day . ' ' . $month . ' ' . $year . ' г. в ' . $hour . ':' . $min;

	$dif = time() - $time;

	if ($dif < 59) {
		return $dif . " сек. назад";
	} elseif ($dif / 60 > 1 and $dif / 60 < 59) {
		return round($dif / 60) . " мин. назад";
	} elseif ($dif / 3600 > 1 and $dif / 3600 < 23) {
		return round($dif / 3600) . " час. назад";
	} else {
		return $date;
	}
}

?>