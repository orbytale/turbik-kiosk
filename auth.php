<?
include('include/init.inc.php');


if (!empty($_POST['login']) && !empty($_POST['passwd'])) {
	$turbik = new turbik($_POST['login'], $_POST['passwd']);
	if (!$turbik->auth) {
		$tpl->assign('error', 1);
	} else {
		redirect('/');
	}
}

$tpl->render('auth.tpl');
?>
