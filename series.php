<?
include('include/init.inc.php');

$serial = db_getRow("SELECT * FROM serials WHERE uri = '".escape_string($_GET['uri'])."'");
if (empty($serial)) die('no serial');


//update automatically once per day
$since_check = time() - $serial['last_checked'];
if ($since_check  > 24 * 3600) {
	$turbik->update_serial( $serial['id'] );
	redirect('/series.php?uri=' . $serial['uri']);
}

$serial['last_check'] = get_last_check($serial['last_checked']);
if (isset($_GET['act']) && $_GET['act'] == 'update') {
	$turbik->update_serial( $serial['id'] );
	redirect('/series.php?uri=' . $serial['uri']);
}



$seasons = array();
$sns = db_getCol("SELECT DISTINCT(season) FROM episodes WHERE serial_id = " . $serial['id']. " ORDER BY season DESC");
foreach ($sns as $sn) {
	$season = array('no' => $sn);
	$season['episodes'] = db_getAll("SELECT * FROM episodes WHERE serial_id = " . $serial['id']. " AND season = $sn ORDER BY episode DESC");
	$seasons[] = $season;

}
$tpl->assign('seasons', $seasons);

$tpl->assign('serial', $serial);
$tpl->render('serial.tpl');
?>
