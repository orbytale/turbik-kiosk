<?
	require_once('../include/init.inc.php');

	if (!empty($_GET['id'])) {
		$id = intval($_GET['id']);
		$episode = db_getRow("SELECT * FROM episodes WHERE id = $id");
		if (!empty($episode)) {
			$meta = $turbik->get_episode_metadata($episode);
			$data = $turbik->decode_meta($meta);
			$links = $turbik->get_all_links($data);

			$tpl->assign('episode', $episode);
			$tpl->assign('links', $links);
			$tpl->render('link_list.tpl');

		}
	}
?>