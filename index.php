<?
include('include/init.inc.php');

if (isset($_GET['refresh'])) {

	$turbik->update_serials_list();
	redirect('/');
}

$serials = db_getAll("SELECT * FROM serials ORDER BY title_en ASC");
$tpl->assign('serials', $serials);
$tpl->render('index.tpl');
?>
