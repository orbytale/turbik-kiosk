<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>{$serial.title_ru}</title>
	<link rel="stylesheet" type="text/css" href="/css/compiled.css"/>
	<link rel="shortcut icon" href="/img/favicon.ico">
</head>
<body style="background:#1a1819 ;">

<div class="everything">
	<div id="header">
		<div class="bg">
			<div class="head-line">
				<a href="/"><span class="logotype"></span></a>

				<a href="/series.php?uri={$serial.uri}">
				<span class="umenu">
					<span class="uptext">{$serial.title_en}</span>
					<span class="botext">{$serial.title_ru}</span>
				</span>
				</a>


				<a href="/series.php?uri={$serial.uri}&act=update">
				<span class="umenu">
					<span class="uptext">Обновить эпизоды</span>
					<span class="botext">Последнее обновление: {$serial.last_check}</span>
				</span>
				</a>

			</div>
		</div>
	</div>
	<div id="series">

		<div class="topimgseries">
			<img src="/images/{$serial.img}" width="980" height="335" alt=""/>

			<div class="sseriestitle">
				<span class="sseriestitlet"><span class="sseriestitleten">{$serial.title_en}</span> / {$serial.title_ru}</span>
			</div>
		</div>
		<div class="content">
			<div class="seasonnum">
				{section name=i loop=$seasons}
					<a data-season="{$seasons[i].no}" class="switch_season"><span class="{if $smarty.section.i.first}seasonnumactive{else}seasonnumnoactive{/if}" >Сезон {$seasons[i].no}</span></a>
				{/section}
			</div>

			{section name=i loop=$seasons}
				<div class="sserieslistbox eplist_season_{$seasons[i].no}" {if !$smarty.section.i.first}style="display: none"{/if}>
					{assign var=eps value=$seasons[i].episodes}
					{section name=j loop=$eps}
						<a class="get_episode" data-id={$eps[j].id}>
						<span class="sserieslistone">
							<span class="sserieslistoneimg">
							<img src="/images/{$serial.uri}/{$eps[j].img}" width="170" height="96" alt=""/>
							<span class="sserieslistoneimgbg"></span>
							<span class="sserieslistoneq">
								{if $eps[j].hd eq '1'}<span class="sserieshq"></span>{/if}
								{if $eps[j].snd_rus eq '1'}<span class="sseriesrsound"></span>            {/if}
								{if $eps[j].snd_en eq '1'}<span class="sseriesesound"></span>            {/if}
								{if $eps[j].srt_rus eq '1'}<span class="sseriesrsub"></span>            {/if}
								{if $eps[j].srt_en eq '1'}<span class="sseriesesub"></span>        {/if}
							</span>
							</span>

							<span class="sserieslistonetxt">
								<span class="sserieslistonetxten">{$eps[j].title_en}</span>
								<span class="sserieslistonetxtru">{$eps[j].title_ru}</span>
								<span class="sserieslistonetxtse">Сезон: {$seasons[i].no}</span>
								<span class="sserieslistonetxtep">Эпизод: {$eps[j].episode}</span>
								<span class="sserieslistonebotc">
									<span class="sserieslistonebotcbg"></span>
								</span>
							</span>
						</span>
						</a>
					{/section}
				</div>
			{/section}




			<div class="sseriesrightbox">
				<div class="sseriesrightblock">
					<span class="sseriesrighttop">Описание сериала</span>
					<span class="sseriesrightc" id="desc">
						<span id="desccnt">{$serial.description}<br/><Br/></span>
					</span>

				</div>
			</div>
		</div>
	</div>
</div>



<div id="modal_content">
	<img src="/img/loader.gif" />
</div>

<script src="/js/jquery-1.9.1.min.js" ></script>
<script src="/js/jquery.simplemodal.js" ></script>
<script src="/js/serial.js" ></script>
</body>
</html>