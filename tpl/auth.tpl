<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Только верующий сможет пройти</title>
	<link rel="stylesheet" type="text/css" href="/css/login.css">
	<link rel="shortcut icon" href="/img/favicon.ico">

</head>
<body>
<div id="login">
	<div class="loginbox">
		<div class="logo"><img src="/img/logotype.png" width="27" height="23" alt="Турбофильм"></div>
		<form action="" method="post" id="signinform">
			<div class="loginblock" id="loginblock">
				<div class="form">
					<span class="left" {if $error eq '1'}style="color:#a32a2a;"{/if}>Логин:</span>
					<span class="rightl">
						<input type="text" class="inputl" id="signinlogin" name="login" tabindex="1" autocapitalize="off">
					</span>
				</div>
				<div class="form">
					<span class="left" {if $error eq '1'}style="color:#a32a2a;"{/if}>Пароль:</span>
					<span class="rightl">
						<input type="password" class="inputl" id="signinpasswd" name="passwd" tabindex="2">
					</span>
				</div>


				<div class="form">
					<span class="a-btcenter"><a id="signinb" tabindex="4" class="a-btl" onclick="document.getElementById('signinform').submit();" >Впустите меня</a></span>
				</div>
			</div>
		</form>

	</div>
</div>

</body>
</html>