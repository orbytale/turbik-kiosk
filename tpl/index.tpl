<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Все сериалы - Турбофильм!</title>
	<link rel="stylesheet" type="text/css" href="/css/compiled.css">
	<link rel="shortcut icon" href="/img/favicon.ico">
</head>
<body class="bodyback">

<div class="everything">
	<div id="header">
		<div class="bg">
			<div class="head-line">
				<a href="/"><span class="logotype"></span></a>
			</div>
		</div>
	</div>
	<div id="series">
		<div class="content">
			<span class="serieslisttitle"><a href="?refresh=1">Сериалы - обновить</a></span>
			<p>
				последнее обновление: {$last_series_check}
			</p>
			{section name=i loop=$serials}
			<a href="/series.php?uri={$serials[i].uri}">
				<span class="serieslistbox">
				<span class="serieslistboxl"><span class="seriesboximgbg"></span>
				<img src="/images/{$serials[i].img}" width="370" height="125" ></span>
				<span class="serieslistboxr">
				<span class="serieslistboxtitle"><span class="serieslistboxen">{$serials[i].title_en}</span><span class="serieslistboxslash"> / </span>
				<span class="serieslistboxru">{$serials[i].title_ru}</span></span>
				<span class="serieslistboxpers">
					<span class="serieslistboxpersl">
						<span class="serieslistboxperstext">Сезонов: {$serials[i].seasons}</span>
						<span class="serieslistboxperstext">Эпизодов: {$serials[i].episodes}</span>
						<span class="serieslistboxperstext">Продолжительность серии: {$serials[i].episode_length}</span>
					</span>
					<span class="serieslistboxpersr">
						<span class="serieslistboxperstext">Премьера сериала: {$serials[i].premier}</span>
							<span class="serieslistboxperstext">Жанр: {$serials[i].genre}</span>
					</span>
				<span class="serieslistboxdesc">{$serials[i].description}</span>
				</span>
				</span>
				<span class="serieslistboxcornerr"></span>
				</span>
			</a>
			{/section}


		</div>
	</div>

</div>


</body>
</html>