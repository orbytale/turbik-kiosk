$(document).ready( function() {
	$('.switch_season').on('click', function () {
		var season = $(this).data('season');
		$('.switch_season span').removeClass('seasonnumactive');
		$('.switch_season span').addClass('seasonnumnoactive');
		$(this).find('span').removeClass('seasonnumnoactive');
		$(this).find('span').addClass('seasonnumactive');

		$('.sserieslistbox').hide();
		$('.eplist_season_' + season).show();
	});

	$('.get_episode').on('click', function() {
		//$('#modal_content').modal();
		$('#modal_content').load('/_ajax/episode.php?id=' + $(this).data('id') ).modal();
	})
})
